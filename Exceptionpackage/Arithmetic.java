package Exceptionpackage;

public class Arithmetic {

	public static void main(String[] args) {
		int no = 10;
		System.out.println("Before division");
		try
		{
			System.out.println(no/0);
			System.out.println("After division");
			
		}
		catch(ArithmeticException e)
		{
			System.out.println("In catch");
			System.out.println(no/0);
			/*e.printStackTrace();*/
		   
		}
		finally
		{
			System.out.println("Finally");
		}
	}

}
