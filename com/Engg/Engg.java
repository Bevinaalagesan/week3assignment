package com.Engg;

public abstract class Engg {
public String name;
public int duration;
public String clgname;
public int percentage;
public Engg(String name, int duration, String clgname, int percentage) {
	super();
	this.name = name;
	this.duration = duration;
	this.clgname = clgname;
	this.percentage = percentage;
}

@Override
public String toString() {
	return "Engg [name=" + name + ", duration=" + duration + ", clgname=" + clgname + ", percentage=" + percentage+ "]";
}

abstract void showSpel();
abstract void showProject();



}
