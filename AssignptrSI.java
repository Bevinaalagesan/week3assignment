import java.util.Scanner;

public class AssignptrSI {

	public static void main(String[] args) {
		float p, r, t, si;
        Scanner s1 = new Scanner(System.in);
        System.out.print("Enter the Principal  ");
        
        p = s1.nextFloat();
        System.out.print("Enter the Rate of interest  ");
        
        r = s1.nextFloat();
        System.out.print("Enter the Time period  ");
        
        t = s1.nextFloat();
        s1.close();
        si = (p * r * t) / 100;
        System.out.print("Simple Interest  " +si);
    
	}

}
