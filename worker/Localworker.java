package worker;

public class Localworker extends Stateworker{
public String cityname;public float citytaxrate;

public Localworker(String name, float federaltaxrate, float PayRate, String statename, float statetaxrate,
		String cityname, float citytaxrate) {
	super(name, federaltaxrate, PayRate, statename, statetaxrate);
	this.cityname = cityname;
	this.citytaxrate = citytaxrate;
}

public float getCitytaxrate() {
	return citytaxrate;
}

public void setCitytaxrate(float citytaxrate) {
	this.citytaxrate = citytaxrate;
}


@Override
public String toString() {
	return "Localworker [cityname=" + cityname + ", citytaxrate=" + citytaxrate + ", statename=" + statename
			+ ", statetaxrate=" + statetaxrate + "]";
}

public double taxWithheld(double grossPayamount)
{   double taxheld=grossPayamount*federaltaxrate;
	double statetaxheld=grossPayamount*statetaxrate;
	double staxheld = statetaxheld+taxheld;
	double localtaxheld=grossPayamount*citytaxrate;
	double ltaxheld=(double)staxheld+localtaxheld;
	return ltaxheld;
}
}