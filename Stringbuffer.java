
public class Stringbuffer {

	public static void main(String[] args) {
		String s1 = "Chennai";
		s1.concat("India");
		StringBuffer s2 = new StringBuffer("test");
		System.out.println(s2);
		s2.append("India");
		System.out.println(s2);
		StringBuilder s3 = new StringBuilder("new");
		System.out.println(s3);
		s3.append("xyz");
		System.out.println(s3);
	}

}
