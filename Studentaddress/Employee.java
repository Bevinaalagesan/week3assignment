package Studentaddress;

public class Employee {
	private int studId;
	private String studName;
	private int salary;
	private Department dept;
	public Employee(int studId, String studName, int salary, Department dept) {
		super();
		this.studId = studId;
		this.studName = studName;
		this.salary = salary;
		this.dept = dept;
	}
@Override
public String toString() {
	return "Employee [studId=" + studId + ", studName=" + studName + ", salary=" + salary + ", dept=" + dept + "]";
}

public void dispDepartment()
{
	System.out.println(dept.deptId+" "+ dept.deptName + " ");
}
	
}